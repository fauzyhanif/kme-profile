<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'kme_profile' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'B15m1ll@h' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'zRTPYr3!6DDW66MU)>b^}U>G!<qKX-v5?:X-)gEiNt*<Vf}6B=S8iXP)E!xmM928');
define('SECURE_AUTH_KEY',  '3JmwaIES_P9(!FKC!h6@cBkR_ma7[CP|<REJSmwrB+a|;)K}[ht8ZLXIEL)ac-}S');
define('LOGGED_IN_KEY',    '=>c$z7U~ls;y?;r<x;+9e((SPrp$# p8?((}=SNSX-KR<mbn>e|p#c~R]AAu6:xI');
define('NONCE_KEY',        'ywVr+NB/@lfCO?gk9FS!,E-WKsW{,=NE@gLRk!=,>9)w+8>&-Uavb#8BQd)ghymW');
define('AUTH_SALT',        'nBM qV0-R^GO-Yb4Fj~sK3 _+;:YXr4O*2y)]laV8e#&1y8&YHx&|C^I=.?|4$6(');
define('SECURE_AUTH_SALT', 'PKtU;hrk8%[_a-muZJl{Rq-6;H},GBFP4m>E;B2?Ow+65VU{6;)~Q5&E^L5bs%:f');
define('LOGGED_IN_SALT',   'eo,78)[FcXS$xE|2wS*gW&eb.jT`#IufJJ$h-eY<}h@=)-q:Uu[%Ysicl{>h6RBE');
define('NONCE_SALT',       'o,y,)IsxCoT<oi]WST4kIurD^?&FUf++BVkD( -|>|aKp9vM+zhap6sO2+:[vJOz');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
